﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (HarmonyAnimation))]
[RequireComponent (typeof (HarmonyRenderer))]
public class HarmonyBehaviour : MonoBehaviour
{
    private HarmonyAnimation _animation;
    private HarmonyAnimation.CallbackEvents _callbackEvents;
    private int _currentAnimationIndex;
    private int _maxAnimationIndexes;

    void Awake()
    {
        _animation = GetComponent<HarmonyAnimation>();
        _maxAnimationIndexes = GetComponent<HarmonyRenderer>().clipNames.Length - 1;
        _currentAnimationIndex = -1;

        _callbackEvents = new HarmonyAnimation.CallbackEvents();
        _callbackEvents.AddCallbackAtEnd(sender =>
        {
            StartCoroutine(RunNextAnimation());
        });
    }

    void Start()
    {
        StartCoroutine(RunNextAnimation());
    }

    public IEnumerator RunNextAnimation()
    {
        if (++_currentAnimationIndex > _maxAnimationIndexes)
        {
            _currentAnimationIndex = 0;
        }

        HarmonyRenderer renderer = GetComponent<HarmonyRenderer>();
        if (renderer == null)
        {
            Debug.LogError("missing harmony renderer component");
            yield break;
        }

        HarmonyAudio audio = GetComponent<HarmonyAudio>();
        if (audio == null)
        {
            Debug.LogError("missing harmony audio component");
            yield break;
        }

        HarmonyAnimation animation = GetComponent<HarmonyAnimation>();
        if (audio == null)
        {
            Debug.LogError("missing harmony animation component");
            yield break;
        }

        renderer.LoadClipIndex(_currentAnimationIndex);
        yield return StartCoroutine(audio.WaitForDownloads());

        animation.ResetAnimation();
        animation.PlayAnimation(24.0f, _currentAnimationIndex, 2.0f, 1, _callbackEvents);
        animation.ResumeAnimation();
    }

}
