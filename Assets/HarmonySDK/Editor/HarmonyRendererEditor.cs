using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(HarmonyRenderer))]
[CanEditMultipleObjects]
public class HarmonyRendererEditor : Editor
{
  int TAB_SIZE = 20;

  SerializedProperty targetProjectFolder;
  SerializedProperty targetClipNames;

  SerializedProperty targetSheetResolution;
  SerializedProperty targetColor;
  SerializedProperty targetDepth;

  SerializedProperty targetRenderTarget;
  SerializedProperty targetRenderTextureViewport;
  SerializedProperty targetSyncCollider;
  SerializedProperty targetColliderShape;
  SerializedProperty targetColliderTrigger;

  static bool foldRenderTarget = false;
  static bool foldColliders = false;

  void OnEnable()
  {
    //  Project properties
    targetProjectFolder = serializedObject.FindProperty("projectFolder");
    targetClipNames = serializedObject.FindProperty("clipNames");

    //  Dynamic properties
    targetSheetResolution = serializedObject.FindProperty("sheetResolution");
    targetColor = serializedObject.FindProperty("color");
    targetDepth = serializedObject.FindProperty("depth");

    //  Render target
    targetRenderTarget = serializedObject.FindProperty("renderTarget");
    targetRenderTextureViewport = serializedObject.FindProperty("renderTextureViewport");

    //  Colliders
    targetSyncCollider = serializedObject.FindProperty("syncCollider");
    targetColliderShape = serializedObject.FindProperty("colliderShape");
    targetColliderTrigger = serializedObject.FindProperty("colliderTrigger");
  }

  public override void OnInspectorGUI()
  {
    Object[] targetObjects = serializedObject.targetObjects;

    EditorGUILayout.PropertyField(targetProjectFolder, new GUIContent("Project Folder"));
    EditorGUILayout.PropertyField(targetClipNames, new GUIContent("Clip Names"), true);

    EditorGUILayout.PropertyField(targetSheetResolution, new GUIContent("Sheet Resolution"));
    EditorGUILayout.PropertyField(targetColor, new GUIContent("Color"));
    EditorGUILayout.PropertyField(targetDepth, new GUIContent("Depth"));

    foldRenderTarget = EditorGUILayout.Foldout(foldRenderTarget, "Render Target");
    if(foldRenderTarget)
    {
      ++EditorGUI.indentLevel;

      EditorGUILayout.PropertyField(targetRenderTarget, new GUIContent("Target"));
      EditorGUILayout.PropertyField(targetRenderTextureViewport, new GUIContent("Texture Viewport"));

      --EditorGUI.indentLevel;
    }

    foldColliders = EditorGUILayout.Foldout(foldColliders, "Colliders");
    if(foldColliders)
    {
      ++EditorGUI.indentLevel;

      EditorGUILayout.PropertyField(targetSyncCollider, new GUIContent("Sync Collider"));
      EditorGUILayout.PropertyField(targetColliderShape, new GUIContent("Collider Shape"));
      EditorGUILayout.PropertyField(targetColliderTrigger, new GUIContent("Collider Trigger"));

      EditorGUILayout.BeginHorizontal();
      GUILayout.Space(EditorGUI.indentLevel * TAB_SIZE);

      if ( !Application.isPlaying )
      {
        if(GUILayout.Button("Generate Colliders", GUILayout.Width(120) ))
        {
          foreach(HarmonyRenderer harmonyRenderer in targetObjects)
          {
            harmonyRenderer.PreCalculateColliders();
          }
        }

        if(GUILayout.Button("Clear Colliders", GUILayout.Width(120)))
        {
          foreach(HarmonyRenderer harmonyRenderer in targetObjects)
          {
            harmonyRenderer.ClearColliders();
          }
        }
      }

      EditorGUILayout.EndHorizontal();


      --EditorGUI.indentLevel;
    }

    serializedObject.ApplyModifiedProperties();

    if (GUI.changed) {
      foreach (Object target in targetObjects)
      {
        EditorUtility.SetDirty(target);
      }
    }
  }
}
