using UnityEditor;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;

using XML_ProjectLoader;

public class CreateHarmonyTextureObject : MonoBehaviour
{
  static string projectFolder = "";

  // Add a menu item named "Do Something" to MyMenu in the menu bar.
  [MenuItem ("GameObject/Harmony/Harmony Texture Object", false, 10)]
  static void Run()
  {
    projectFolder = EditorUtility.OpenFolderPanel("Import Harmony Game Engine Project", projectFolder, "" );
    if ( !string.IsNullOrEmpty(projectFolder) &&
         new DirectoryInfo(projectFolder).Exists )
    {
      string[] clipNames = XML_StageLoader.loadStageClipNames(projectFolder).ToArray();

      if ( clipNames.Length > 0 )
      {
        string name = Path.GetFileName( projectFolder );

        GameObject rendererObject = new GameObject( name );
        rendererObject.transform.localScale = new Vector3( 10.0f, 10.0f, 10.0f );

        HarmonyRenderer renderer = rendererObject.AddComponent<HarmonyRenderer>();
        /*HarmonyAnimation animation = */rendererObject.AddComponent<HarmonyAnimation>();
        /*HarmonyAudio audio = */rendererObject.AddComponent<HarmonyAudio>();

        /*AudioSource audioSource = */rendererObject.AddComponent<AudioSource>();

        MeshFilter meshFilter = rendererObject.AddComponent<MeshFilter>();
        MeshRenderer meshRenderer = rendererObject.AddComponent<MeshRenderer>();

        //  Remove streaming assets path part of the specified project folder if
        //  applicable.  An absolute path will work on the user's machine but will
        //  likely not be found elsewhere.
        string streamingAssetsPath = Application.streamingAssetsPath;
        if ( projectFolder.Contains( streamingAssetsPath ) )
        {
          projectFolder = projectFolder.Substring( streamingAssetsPath.Length + 1 );
        }

        renderer.projectFolder = projectFolder;
        renderer.clipNames = clipNames;
        renderer.renderTarget = HarmonyRenderer.RenderTarget.eRenderTexture;
        renderer.renderTextureViewport = new Rect(-512,-512,1024,1024);

        //  Create basic quad to map texture unto.
        Mesh mesh = new Mesh();

        Vector3[] vertices = new Vector3[4]
        {
          new Vector3( -0.5f, -0.5f, 0),
          new Vector3(  0.5f, -0.5f, 0),
          new Vector3(  0.5f,  0.5f, 0),
          new Vector3( -0.5f,  0.5f, 0)
        };

        Vector3[] normals = new Vector3[4]
        {
          new Vector3( 0, 0, -1.0f ),
          new Vector3( 0, 0, -1.0f ),
          new Vector3( 0, 0, -1.0f ),
          new Vector3( 0, 0, -1.0f )
        };

        Vector2[] uvs = new Vector2[4]
        {
          new Vector2( 0, 1 ),
          new Vector2( 1, 1 ),
          new Vector2( 1, 0 ),
          new Vector2( 0, 0 )
        };

        int[] faces = new int[6]
        {
          2, 1, 0,
          3, 2, 0
        };

        mesh.vertices = vertices;
        mesh.normals = normals;
        mesh.uv = uvs;
        mesh.triangles = faces;

        meshFilter.mesh = mesh;

        //  Create material for new mesh.
        Shader shader = Shader.Find("Harmony/UnlitTransparentPremult");
        if (shader != null)
        {
          Material mat = new Material(shader);
          mat.name = name + "_mat";

          meshRenderer.material = mat;
        }

        //  If no Harmony cameras available, ask user if he wants to create one.
        HarmonyCamera[] harmonyCameras = FindObjectsOfType<HarmonyCamera>();
        if (harmonyCameras.Length == 0)
        {
          string title = "Create HarmonyCamera components?";
          string body = "Only a camera with the HarmonyCamera component will render Harmony Game Objects.";

          if ( EditorUtility.DisplayDialog( title, body, "Create", "Do Not Create") )
          {
            //  Make sure there is at least one camera in the scene.
            Camera[] cameras = FindObjectsOfType<Camera>();
            if (cameras.Length == 0)
            {
              GameObject cameraObject = new GameObject( "Main Camera" );
              /*Camera camera = */cameraObject.AddComponent<Camera>();
            }

            CreateHarmonyCamera.ShowWindow();
          }
        }
      }
    }
  }
}
