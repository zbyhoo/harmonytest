
#if (UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
#define UNITY_MULTITHREADED
#else
#undef  UNITY_MULTITHREADED
#endif

using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

[ExecuteInEditMode]
public class HarmonyCamera : MonoBehaviour
{

  #if (UNITY_IPHONE || UNITY_XBOX360) && !UNITY_EDITOR

    #if !UNITY_MULTITHREADED
      [DllImport ("__Internal")]
      public static extern void InitializeRenderer (int deviceType);

      [DllImport ("__Internal")]
      public static extern void UnityRenderEvent (int eventID);
    #endif // !UNITY_MULTITHREADED

    [DllImport ("__Internal")]
    private static extern int CreateCameraEvent( int unityFrame, int cullingMask, float[] projection, float[] view);

  #else // !((UNITY_IPHONE || UNITY_XBOX360) && !UNITY_EDITOR)

    #if !UNITY_MULTITHREADED // Use for android
      [DllImport ("HarmonyRenderer")]
      public static extern void InitializeRenderer (int deviceType);

      [DllImport ("HarmonyRenderer")]
      public static extern void UnityRenderEvent (int eventID);
    #endif // !UNITY_MULTITHREADED

    [DllImport ("HarmonyRenderer")]
    private static extern int CreateCameraEvent( int unityFrame, int cullingMask, float[] projection, float[] view);

  #endif

  enum GfxDeviceRenderer
  {
    kGfxRendererOpenGL = 0,          // OpenGL
    kGfxRendererD3D9,                // Direct3D 9
    kGfxRendererD3D11,               // Direct3D 11
    kGfxRendererGCM,                 // Sony PlayStation 3 GCM
    kGfxRendererNull,                // "null" device (used in batch mode)
    kGfxRendererHollywood,           // Nintendo Wii
    kGfxRendererXenon,               // Xbox 360
    kGfxRendererOpenGLES,            // OpenGL ES 1.1
    kGfxRendererOpenGLES20Mobile,    // OpenGL ES 2.0 mobile variant
    kGfxRendererMolehill,            // Flash 11 Stage3D
    kGfxRendererOpenGLES20Desktop,   // OpenGL ES 2.0 desktop variant (i.e. NaCl)
    kGfxRendererCount
  };

  public enum RenderOrder
  {
    ePreCamera,
    ePostCamera,
    eEndOfFrame
  };

  public RenderOrder renderOrder = RenderOrder.ePostCamera;

  private bool       renderingWithDirectX = false;

  private static int HARMONY_RENDERER_CLEANUP = 1;
  //private static int HARMONY_RENDERER_FINAL_CLEANUP = 2;
  private static int HARMONY_RENDERER_RENDER_TEXTURE = 3;

  IEnumerator Start()
  {
    PluginInitialize();

    //Message.Log( "Graphics Device Version: " + SystemInfo.graphicsDeviceVersion );
    if ( SystemInfo.graphicsDeviceVersion.Contains( "Direct3D" ) )
      renderingWithDirectX = true;

    yield return StartCoroutine("CallPluginAtEndOfFrames");
  }

  private void OnPreCull()
  {
    //  Somehow using OnPreRender to call a plugin event makes other
    //  game object disappear on Windows.  Still need to investigate.

    //  Render selected game object to texture before Unity renders
    //  its own geometry so that texture is up to date at that time.
    PluginRenderToTexture();
  }

  private void OnPreRender()
  {
    //  Will render plugin before scene renders itself.  Don't
    //  clear with skybox if you want to see something.
    if ( renderOrder == RenderOrder.ePreCamera )
    {
      PluginRenderFrame();
    }
  }

  private void OnPostRender()
  {
    //  Will render plugin after scene renders itself but before
    //  ui renders.
    if ( renderOrder == RenderOrder.ePostCamera )
    {
      PluginRenderFrame();
    }
  }

  private void OnApplicationQuit()
  {
// #if UNITY_MULTITHREADED
//     GL.IssuePluginEvent (HARMONY_RENDERER_FINAL_CLEANUP);
// #else
//     UnityRenderEvent(HARMONY_RENDERER_FINAL_CLEANUP);
// #endif
  }

  private IEnumerator CallPluginAtEndOfFrames ()
  {
    while (true) {

      // Wait until all frame rendering is done
      yield return new WaitForEndOfFrame();

      //  Will render plugin after all cameras finished rendering and
      //  after ui.
      if ( renderOrder == RenderOrder.eEndOfFrame )
      {
        PluginRenderFrame();
      }
    }
  }

  private void PluginInitialize()
  {
#if !UNITY_MULTITHREADED

    InitializeRenderer( (int)GfxDeviceRenderer.kGfxRendererOpenGLES20Mobile );
    GL.InvalidateState();

#endif
  }

  private void PluginRenderFrame()
  {
    Camera camera = GetComponent<Camera>();
    Matrix4x4 projectionMatrix = camera.projectionMatrix;
    Matrix4x4 viewMatrix = camera.worldToCameraMatrix;

    //  Unless we find a better way of handling that case.
    //  Deferred lighting in unity renders in an offline render target and then
    //  flips it (along with our plugin rendering) in the final render.
    if ( (camera.actualRenderingPath == RenderingPath.DeferredLighting) &&
         renderingWithDirectX )
    {
      Matrix4x4 scaleMatrix = Matrix4x4.Scale( new Vector3(1.0f, -1.0f, 1.0f) );
      projectionMatrix = projectionMatrix * scaleMatrix;
    }

    int unityFrame = Time.frameCount;
    int renderId = CreateCameraEvent( unityFrame, camera.cullingMask, MatrixToArray(projectionMatrix), MatrixToArray(viewMatrix) );

#if UNITY_MULTITHREADED
    GL.IssuePluginEvent(renderId);
#else
    UnityRenderEvent(renderId);
    GL.InvalidateState();
#endif 
  }

  public static void PluginCleanup()
  {
    //  Ask for opengl buffers cleanup in its own thread.
#if UNITY_MULTITHREADED
    GL.IssuePluginEvent(HARMONY_RENDERER_CLEANUP);
#else
    UnityRenderEvent(HARMONY_RENDERER_CLEANUP);
#endif
  }

  public static void PluginRenderToTexture()
  {
#if UNITY_MULTITHREADED
    GL.IssuePluginEvent(HARMONY_RENDERER_RENDER_TEXTURE);
#else
    UnityRenderEvent(HARMONY_RENDERER_RENDER_TEXTURE);
    GL.InvalidateState();
#endif
  }

  private static float[] MatrixToArray(Matrix4x4 matrix)
  {
    float[] result = new float[16];
    for (int row = 0; row < 4; row++)
    {
      for (int col = 0; col < 4; col++)
      {
        result[col + row * 4] = matrix[col, row];
      }
    }

    return result;
  }

}
