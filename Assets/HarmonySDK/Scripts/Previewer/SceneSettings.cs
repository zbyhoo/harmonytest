
using UnityEngine;

public class SceneSettings : MonoBehaviour
{
  public string     projectFolder;
  public string[]   clipNames;
  public int        clipIdx;

  public GameObject viewerGroup;
  public GameObject browserGroup;
}
