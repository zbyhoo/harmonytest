
using UnityEngine;
using System.Collections;

public class HarmonyRendererController : MonoBehaviour {

  private GameObject rendererObject;
  public float       frameRate = 24.0f;

  IEnumerator Start()
  {
    SceneSettings settings = FindObjectOfType(typeof(SceneSettings)) as SceneSettings;
    if ( settings != null )
    {
      if ( settings.clipNames.Length > 0 )
      {
        rendererObject = new GameObject( "RendererObject" );
        rendererObject.transform.parent = settings.viewerGroup.transform;
        rendererObject.transform.localScale = new Vector3( 0.01f, 0.01f, 0.01f );  //  fixme .. shouldn't have to rescale the object.

        HarmonyRenderer renderer = rendererObject.AddComponent<HarmonyRenderer>();
        HarmonyAnimation animation = rendererObject.AddComponent<HarmonyAnimation>();
        HarmonyAudio audio = rendererObject.AddComponent<HarmonyAudio>();
        AudioSource templateAudioSource = rendererObject.AddComponent<AudioSource>();

        //  Linear rolloff.  Easier for sound to be heard.
        templateAudioSource.rolloffMode = AudioRolloffMode.Linear;

        //  Set up clip collection in renderer.
        renderer.projectFolder = settings.projectFolder;
        renderer.clipNames = settings.clipNames;

        renderer.LoadClipIndex(settings.clipIdx);

        //  Make sure sound is all downloaded before playing animation.
        yield return StartCoroutine(audio.WaitForDownloads());

        //  Loop animation indefinitely.
        animation.ResetAnimation();
        animation.LoopAnimation( frameRate, settings.clipIdx );
      }
    }
  }

  public IEnumerator RefreshRendererObject()
  {
    if ( rendererObject == null )
      yield break;

    SceneSettings settings = FindObjectOfType(typeof(SceneSettings)) as SceneSettings;
    if ( settings == null )
      yield break;

    HarmonyRenderer renderer = rendererObject.GetComponent<HarmonyRenderer>();
    HarmonyAnimation animation = rendererObject.GetComponent<HarmonyAnimation>();
    HarmonyAudio audio = rendererObject.GetComponent<HarmonyAudio>();
    if ( (renderer == null) || (animation == null) || (audio == null) )
      yield break;

    renderer.LoadClipIndex(settings.clipIdx);

    //  Make sure sound is all downloaded before playing animation.
    yield return StartCoroutine(audio.WaitForDownloads());

    //  Loop animation indefinitely.
    animation.ResetAnimation();
    animation.LoopAnimation( frameRate, settings.clipIdx );
    animation.ResumeAnimation();  // resume if paused.
  }

  public void OnDestroy()
  {
    Destroy(rendererObject);
  }
}
