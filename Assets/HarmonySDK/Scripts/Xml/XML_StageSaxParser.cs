
using System.Collections.Generic;
using System.Globalization;
using System;

using XML_Types;
using XML_SaxParser;
using XML_ProjectLoader;

namespace XML_StageSaxParser
{
  /*!
   * Retrieve list of clip names available in project
   */
  public class XML_StageNamesSaxParserComponent : XML_SaxParserComponent
  {
    enum State
    {
      eRoot,
      eStages,
      eStage
    };

    public XML_StageNamesSaxParserComponent()
    {
      _clipNames = new List<string>();
    }

    public override XML_SaxParserComponent startElement( string nodeName, List<XML_Attribute> attributes )
    {
      if ( _state == State.eRoot )
      {
        if ( nodeName.Equals( XML_Constants.kStageColTag ) )
        {
          _state = State.eStages;
        }
      }
      else if ( _state == State.eStages )
      {
        if ( nodeName.Equals( XML_Constants.kStageTag ) )
        {
          //  Only enter stage mode if we encounter the xml tag we search for.
          string clipName = findAttribute( attributes, XML_Constants.kNameTag );
          if ( !string.IsNullOrEmpty(clipName) )
          {
            _clipNames.Add(clipName);
            _state = State.eStage;
          }
        }
      }

      return this;
    }

    public override void endElement( string nodeName )
    {
      if ( nodeName.Equals( XML_Constants.kStageTag ) )
      {
        _state = State.eStages;
      }
      else if ( nodeName.Equals( XML_Constants.kStageColTag ) )
      {
        _state = State.eRoot;
      }
    }

    public List<string> clipNames
    {
      get { return _clipNames; }
    }

    private State        _state;
    private List<string> _clipNames;
  }

  /*!
   * Retrieve list of sound play sequences in clip
   */
  public class XML_AudioSaxParserComponent : XML_SaxParserComponent
  {
    enum State
    {
      eRoot,
      eStages,
      eStage
    };

    public XML_AudioSaxParserComponent( string clipName )
    {
      _clipName = clipName;
      _state = State.eRoot;

      _soundSequences = new List< XML_Types.XML_SoundSequence >();
    }

    public override XML_SaxParserComponent startElement( string nodeName, List<XML_Attribute> attributes )
    {
      if ( _state == State.eRoot )
      {
        if ( nodeName.Equals( XML_Constants.kStageColTag ) )
        {
          _state = State.eStages;
        }
      }
      else if ( _state == State.eStages )
      {
        if ( nodeName.Equals( XML_Constants.kStageTag ) )
        {
          //  Only enter stage mode if we encounter the xml tag we search for.
          string value = findAttribute(attributes, XML_Constants.kNameTag);
          if ( !string.IsNullOrEmpty(value) && value.Equals(_clipName) )
          {
            _state = State.eStage;
          }
        }
      }
      else // if ( _state == eStage )
      {
        if ( nodeName.Equals( XML_Constants.kSoundTag ) )
        {
          XML_Types.XML_SoundSequence seq = new XML_Types.XML_SoundSequence();
          seq._startFrame = 1.0f;

          foreach (XML_Attribute attr in attributes)
          {
            if ( attr._name.Equals( XML_Constants.kNameTag ) )
              seq._name = attr._value;
            else if ( attr._name.Equals( XML_Constants.kTimeTag ) )
              seq._startFrame = float.Parse( attr._value, CultureInfo.InvariantCulture.NumberFormat);
          }

          if ( !string.IsNullOrEmpty(seq._name) )
          {
            _soundSequences.Add(seq);
          }
        }
      }

      return this;

    }

    public override void endElement( string nodeName )
    {
      if ( nodeName.Equals( XML_Constants.kStageTag ) )
      {
        _state = State.eStages;
      }
      else if ( nodeName.Equals( XML_Constants.kStageColTag ) )
      {
        _state = State.eRoot;
      }
    }

    private string                 _clipName;
    private State                  _state;

    private List< XML_Types.XML_SoundSequence >  _soundSequences;

    public List< XML_Types.XML_SoundSequence > soundSequences
    {
      get { return _soundSequences; }
    }

  }
}
