
#ifndef _RD_RENDER_ID_H_
#define _RD_RENDER_ID_H_

namespace RD_RenderId
{
  int uniqueId();
}

#endif /* _RD_RENDER_ID_H_ */
