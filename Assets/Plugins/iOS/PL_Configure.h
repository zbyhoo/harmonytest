#ifndef _PL_CONFIGURE_H_
#define _PL_CONFIGURE_H_

// Target platform
#define TARGET_IOS

// Rendering engine
#define SUPPORT_RENDERER_GLES2

#endif /* _PL_CONFIGURE_H_ */
